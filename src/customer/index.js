const express = require('express');
const customer = require('./actions');
const app = express.Router();

//
module.exports = () => {

  // Create
  app.post('/', customer.create);  //problem???

  // Find By Id
  app.get('/:store_id/:customer_id', customer.load);
  
  // Update
  app.put('/:id', customer.update);

  // Create Credit Card
  app.post('/creditCard', customer.createCustomerCreditCard);

  // Remove Credit Card
  app.delete('/creditCard', customer.removeCustomerCreditCard)

  return app;
};