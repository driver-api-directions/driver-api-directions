const enliteApi = require('../utils/enlite-api');

/*
  {
    store_id: "",
    user: {
      some use unformation
    }
  }
*/
const create = async (req, res, next) => {
  try {
    const { user, store_id } = req.body;

    const token = await enliteApi.getStoreToken(store_id);
    
    const newCustomer = await enliteApi.createCustomer(user, token);

    res.json(newCustomer);
  }
  catch (err) {
    next(err);
  }
};

const load = async (req, res, next) => {
  try {
    const { store_id, customer_id } = req.params;
    const token = req.headers['authorization'];

    const customer = await enliteApi.loadCustomer(customer_id, store_id, token);

    res.json(customer);
  }
  catch (err) {
    next(err);
  }
};

/*
  {
    "store_id": "",
    updatedCustomer: {
      // some info
    }
  }
*/
const update = async (req, res, next) => {
  try {
    const customer_id = req.params.id;
    const { updatedCustomer, store_id } = req.body;
    const token = req.headers['authorization'];

    const customer = await enliteApi.updateCustomer(customer_id, store_id, updatedCustomer, token);

    res.json(customer);
  }
  catch (err) {
    next(err);
  }
};

/*
  {
    customer_id: "2464",
    store_id: "1",
    firm_id: "1",
    card_id: "4242424242424242"
  }
*/
//TODO: learn how the  credit card process is, then fix this
const createCustomerCreditCard = async (req, res, next) => {
  try {
    const { customer_id, firm_id, store_id } = req.body;
    const token = req.headers['authorization'];

    const card_id = await enliteApi.createIdCustomerCard(customer_id, store_id, token);

    const creditCard = await enliteApi.createCreditCard(customer_id, firm_id, store_id, card_id, token);

    res.json(creditCard);
  }
  catch (err) {
    next(err);
  }
};

/*
  {
    customer_id: "GbDvjl:L",
    card_id: "HjkLlioioijohklkj"
  }
*/
//TODO: learn how the  credit card process is, then fix this
const removeCustomerCreditCard = async (req, res, next) => {
  try {
    const { customer_id, card_id } = req.body;

    const deletedCard = await enliteApi.removeCreditCard(customer_id, card_id);

    res.json(deletedCard);
  }
  catch (err) {
    next(err);
  }

};

//
module.exports = {
  create,
  load,
  update,
  createCustomerCreditCard,
  removeCustomerCreditCard
};

