const _ = require('lodash');
const request = require('axios');
const jwtDecode = require('jwt-decode');
const config = require('../../config');
const md5 = require('md5');

const headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
};

const daysOfWeek = {
  'en': {
    '0': 'Su',
    '1': 'Mo',
    '2': 'Tu',
    '3': 'We',
    '4': 'Th',
    '5': 'Fr',
    '6': 'Sa'
  }
}

//TODO: check if we need this or not
const requiredParams = () => {
  const { token, store_id } = config.reqParams
  return {
    token,
    store_id
  }
}

//TODO: check if we need this or not
const setRequiredParams = ({ token, store_id }) => {  
  config.reqParams = Object.assign({}, { token, store_id })
}

const emailAuthentication = async (email) => { 
  try {
    if (_.isEmpty(email))
      throw new Error(`Invalid email`)

    const response = await request.post(`${config.api}/user-customer-register`, { email })

    return response.data.data || {}
  }
  catch (err) {
    console.log(err.message)
    console.log(err.stack)
    throw new Error(`Email registration failed`)
  }
}

const verifyCode = async (code, email) => {
  try {
    if (_.isEmpty(code))
      throw new Error(`Invalid code`)

    const response = await request.post(`${config.api}/user-customer-validate`, {
      customer_register_code: code,
      email
    })

    const stores = response.data.data || []
    
    return stores.map(store => {
      return {
        store_id: store.store_id,
        firm_id: store.firm_id,
        user_id: store.user_id,
        name: store.store_name,
        token: store.token
      }
    })
  }
  catch(err) {
    console.log(err.message)
    console.log(err.stack)
    throw new Error(`Verification code not recognized`)
  }
}

//TODO: use them if are  needed
// export const checkRouteLicense = async (store_id) => { //why it has 2 store_id
//   const params = {
//     ...requiredParams(),
//     store_id
//   }

//   try {

//     const response = await request.get(`${config.api}/firm-license`, {
//       params,
//       headers
//     })

//     return response.data.data

//   } catch (err) {
//     console.log(err.message)
//     console.log(err.stack)
//     return []
//   }
// }

// export const getPaymentSettings = async (store_id, firm_id) => {//store_id
//   const params = {
//     ...requiredParams(),
//     store_id,
//     firm_id,
//     add_join: 'PaymentType'
//   }

//   try {

//     const response = await request.get(`${config.api}/store-payment-type`, {
//       params,
//       headers
//     })

//     return response.data.data

//   } catch (err) {
//     console.log(err.message)
//     console.log(err.stack)
//     return []
//   }
// }
// /**
//  *
//  * @param {*} param0
//  */

function getServices (items) {
  const services = {
    d : { items : [], name : 'Dry Cleaning' },
    e : { items : [], name : 'Etc' },
    l : { items : [], name : 'Laundry' },
    o : { items : [], name : 'Outside' },
    t : { items : [], name : 'Tailoring' },
    p : { items : [], name : 'Press' },
    x : { items : [], name : 'Other' },
  };

  _.each(items, (item) => {
    const categoryCode = item.item_category_name_code ? item.item_category_name_code.split('-')[0] : 'x';
    if (item.item) {
      services[categoryCode].items.push(item);
    }
  });

  return _.filter(services, (service) => service.items.length > 0);
}

const loadStore = async (store_id, firm_id, token) => {
  try {
    const routes = await getStoreRoutes(store_id, firm_id, token);
    const starches = await getStoreStarches(store_id, firm_id, token);
    const promotions = await getPromotions(store_id, token);
    const storeToken = await getStoreToken(store_id);
    const layouts =  await getLayout(store_id, token);

    const layoutApp = _.find(layouts, { 'name': 'customer_app_items' });
    const items = await getLayoutItems(store_id, layoutApp.id, token);
    const services = getServices(items);

    const store = Object.assign({}, 
      { routes },
      { starches },
      { promotions },
      { storeToken },
      { layouts },
      { services},
    );

    return store;
  }
  catch (err) {
    console.log(err.message)
    console.log(err.stack)
    return []
  }
}

const getStoreRoutes = async (store_id, firm_id, token) => {
  const params = {
    token,
    add_join: 'RouteDay',
    firm_id,
    store_id
  }

  try {
    const response = await request.get(`${config.api}/route`, {
      params,
      headers
    })

    response.data.data.forEach(r => r.route_day.forEach(rd => {
      rd.day_label = rd.day.split(',').map(d => daysOfWeek['en'][d.trim()]).join(',')
    }))

    return response.data.data

  } catch (err) {
    console.log(err.message)
    console.log(err.stack)
    return []
  }
}

const loadStoreRoute = async (route_id, store_id, firm_id, token) => {
  const params = {
    route_id,
    firm_id,
    store_id,
    token
  }
  
  try {
    const response = await request.post(`${config.api}/route-manifest-summary?token=${token}`,
      params,
      headers
    )

    response.data.data.routeData.forEach(r => r.route_day.forEach(rd => {
      rd.day_label = rd.day.split(',').map(d => daysOfWeek['en'][d.trim()]).join(',')
    }))

    return response.data.data

  } catch (err) {
    console.log(`[ERROR] getStoreRoutes request failed:`, JSON.stringify({
      method: `${config.api}/route-manifest-summary`,
      params,
      headers,
      response: err.response || {}
    }))
    console.log(err.message)
    console.log(err.stack)
    return []
  }
}

const getStoreToken = async (store_id) => { 
  try {
    const response = await request.post(`${config.api}/store-public-token`, {
      storePublicToken: md5(String(store_id))
     }, {
      headers
    })

    return response.data.data

  } catch (err) {
    console.log(err.message)
    console.log(err.stack)
    return []
  }
}

const getStoreStarches = async (store_id, firm_id, token) => {
  try {
    const response = await request.get(`${config.api}/store-detail?category=starch&store_id=${store_id}&firm_id=${firm_id}&token=${token}`, {
      headers
    })

    return response.data.data

  } catch (err) {
    console.log(err.message)
    console.log(err.stack)
    return []
  }
}

//TODO: check the store setting to see if it sets to enlite reward or other things
const getPromotions = async (store_id, token) => {
  console.log(store_id);
  console.log(token);
  const params = {
    token,
    store_id
  }

  try {
    const response = await request.get(`${config.api}/store-coupon`, {
      params,
      headers
    })

    return response.data.data
  }
  catch(err) {
    console.log(err.message)
    console.log(err.stack)
    return []
  }
}

const storeFeedback = async (store_id, user_id, token, message) => {
  const params = {
    store_id,
    user_id,
    message
  }

  try {
    const response = await request.post(`${config.api}/alert-message?token=${token}`, {
      params,
      headers
    })

    return response.data.data

  } catch (err) {
    console.log(err.message)
    console.log(err.stack)
    return []
  }
}

const loadCustomer = async (customer_id, store_id, token) => {

  const info = await getCustomerInfo(customer_id, store_id, token);
  const creditCards = await getCreditCards(customer_id, store_id, token);
  const customerRoutes = await getCustomerRoutes(store_id, customer_id, token);
  const rewards = await getCustomerRewards();
  const tickets = await getTickets(customer_id, store_id, token);
  

  let customer = Object.assign({}, {"info": info}, {"credit cards": creditCards}, {"routes": customerRoutes}, {"rewards": rewards}, {"tickets": tickets});
 
  return customer;
}

//This method  should not have try catch. so if create user failed client would know.
//TODO: find a better  way
const createCustomer = async (user, token) => {
    const response = await request.post(`${config.api}/public-user?token=${token}`, user, {
      headers
    })
   console.log(`Response: > ${JSON.stringify(response,0,2)}`);
    
    return response.data.data
}

const getCustomerInfo = async (id = 0, store_id, token) => {

  const params = {
    store_id,
    token,
    id,
    add_join: 'UserDetail'
  }

  try {
    const response = await request.get(`${config.api}/user-customer`, {
      params,
      headers
    })

    let profile = response.data.data
    if (_.isArray(profile)) {  //why? I dont like it
      profile = profile[0]
    }

    return profile
  }
  catch(err) {
    console.log(err.message)
    console.log(err.stack)
    return []
  }
}

const getCreditCards = async (user_id = 0, store_id, token) => {
  const params = {
    store_id,
    token,
    user_id,
    function: 'customerCards'
  }

  try {
    const response = await request.get(`${config.api}/starlite`, {
      params,
      headers
    })

    return response.data.data

  } catch (err) {
    console.log(err.message)
    console.log(err.stack)
    return []
  }
}

const getCustomerRoutes = async (store_id, customer_id, token) => {
  try {
    let response = await request.get(`${config.api}/customer-route/?customer_id=${customer_id}&store_id=${store_id}&token=${token}`, {
      headers
    })
    
    return response.data.data

  } catch (err) {
    console.log(err.message)
    console.log(err.stack)
    return []
  }
}

const getCustomerRewards = async () => { 
  console.log('No Data For Customer Rewards');
}

const getTickets = async (customer_id = 0, store_id, token) => {
  const params = {
    store_id,
    token,
    add_join: 'invoice.InvoiceItem',
    customer_id
  }

  try {
    const response = await request.get(`${config.api}/ticket`, {
      params,
      headers
    })

    return response.data.data
  }
  catch(err) {
    console.log(err.message)
    console.log(err.stack)
    return []
  }
}

const updateCustomer = async (customer_id, store_id, customer, token) => {
  const params = {
    store_id,
    token
  }

  try {
    const response = await request.put(`${config.api}/user/${customer_id}`,customer, {
      params,
      headers
    })

    return response.data.data  

  } catch (err) {
    console.log(err.message)
    console.log(err.stack)
    return []
  }
}

const createIdCustomerCard = async (user_id, store_id, token) => {
  const params = {
    store_id,
    token,
    user_id,
    function: 'customerCreate'
  }

  try {
    const response = await request.get(`${config.api}/starlite`, {
      params,
      headers
    })

    return response.data

  } catch (err) {
    console.log(err.message)
    console.log(err.stack)
    return []
  }
}
const createCreditCard = async (user_id, firm_id, store_id, card_id, token) => {
  const params = {
    store_id,
    token,
    user_id,
    firm_id,
    card_id,
    function: 'customerCardSave'
  }

  try {
    const response = await request.get(`${config.api}/starlite`, {
      params,
      headers
    })

    return response.data.data

  } catch (err) {
    console.log(err.message)
    console.log(err.stack)
    return []
  }
}

// TODO: first  learn how credit card process is, then use these methods
// export const removeCreditCard = async (customer_id = 0, card_id = 0) => {
//   const params = {
//     user_id: customer_id,
//     card_id,
//     function: 'customerCardDelete',
//     ...requiredParams()
//   }

//   try {
//     const response = await request.get(`${config.api}/starlite`, {
//       params,
//       headers
//     })

//     return response.data.data

//   } catch (err) {
//     console.log(err.message)
//     console.log(err.stack)
//     return []
//   }

// }

const createQuickTicket = async (store_id, ticket, token) => { 
  const params = {
    store_id,
  }

  try {
    const response = await request.post(`${config.api}/ticket?token=${token}`, ticket, {
      params,
      headers
    })

    return response.data.data

  } catch (err) {
    console.log(err.message)
    console.log(err.stack)
    return []
  }
}

const getInvoiceStatus = async (store_id, firm_id, token) => {
  const params = {
    token,
    store_id,
    firm_id
  }

  try {

   const response = await request.get(`${config.api}/route-invoice-state`, {
     params,
     headers
   })

   return response.data.data

  } catch (err) {
   console.log(err.message);
   console.log(err.stack);
   return [];
 }
}


const setInvoiceStatus = async (store_id, firm_id, invoice_id, status_id, token) => {
 try {
   
   const response = await request
     .put(`${config.api}/route-invoice/${invoice_id}?store_id=${store_id}&firm_id=${firm_id}&token=${token}`, {
       route_invoice_state_id: status_id
     }, {
       headers
     })

   return response.data.data;

 } catch (err) {
   console.log(err.message);
   console.log(err.stack);
   return [];
 }
}

const ownerLogin = async ({ email, password }) => {
  try {
    if (_.isEmpty(email)) {
      throw new Error('Invalid email');
    }

    const response = await request.post(`${config.api}/user-owner-login`, {
      email,
      password,
    });

    const token = response.data.token;
    const decode  = jwtDecode(token);
    const { user } = decode;
    const { store_id } = user;

    setRequiredParams({ token, store_id });

    return {user, token};
  }
  catch (err) {
    console.log(err.message)
    console.log(err.stack)
    throw new Error(`Email address not recognized`)
  }
}

const getLayout = async (store_id, token) => {
  const params = {
    token,
    store_id
  };

  try {

    const response = await request.get(`${config.api}/layout-item-category`, {
      params,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    });

    return response.data.data;

  } catch (err) {
    console.log(err.message);
    console.log(err.stack);
    return [];
  }
}

const getLayoutItems = async (store_id, layout_id, token) => {
  try {
    const params = {
      token,
      add_join: 'Item',
      store_id,
      layout_item_category_id: layout_id
    };

    const response = await request.get(`${config.api}/layout-item`, {
      params,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    });

    return response.data.data;

  } catch (err) {
    console.log(err.message);
    console.log(err.stack);
    return [];
  }

}

module.exports = { 
  getStoreToken, 
  emailAuthentication, 
  verifyCode, 
  createCustomer, 
  createIdCustomerCard,
  createCreditCard,
  createQuickTicket,
  loadStore, 
  loadStoreRoute,
  loadCustomer, 
  getInvoiceStatus,
  updateCustomer, 
  setInvoiceStatus, 
  storeFeedback ,
  ownerLogin,
  getLayout,
  getLayoutItems,
};