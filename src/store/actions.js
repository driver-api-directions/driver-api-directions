  const enliteApi = require('../utils/enlite-api');
  
  const find = async (req, res, next) => {
    try {
      const { store_id, firm_id } = req.params; 
      const token = req.headers['authorization'];
      const store = await enliteApi.loadStore(store_id, firm_id, token);
    
      res.json(store);
    }
    catch (err) {
      next(err);
    }
  };
  
  /*
    {
      store_id: "Nb878",
      customer_id: "Jhn898",
      message: "Baher is Awesome"
    }
  */
  const feedback = async (req, res, next) => {
    try {
        const { store_id, customer_id, message } = req.body;
        const token = req.headers['authorization'];

        const feedback = await enliteApi.storeFeedback(store_id, customer_id, token, message);
       
        res.json(feedback);
    }
    catch (err) {
      next(err);
    }
  };
  
  const route = async (req, res, next) => {
    try {
      const { store_id, firm_id, route_id } = req.params; 
      const token = req.headers['authorization'];

      const route = await enliteApi.loadStoreRoute(route_id, store_id, firm_id, token);  
      
      res.json(route);
    }
    catch (err) {
      next(err);
    }
  };

  //
  module.exports = {
    find,
    feedback,
    route
  };
  
  