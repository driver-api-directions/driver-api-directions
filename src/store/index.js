const express = require('express');
const store = require('./actions');
const app = express.Router();

//
module.exports = () => {

  // Find By Id
  app.get('/:firm_id/:store_id', store.find);

  // Find Route By Id
  app.get('/:firm_id/:store_id/route/:route_id', store.route);

  // Update
  app.put('/feedback', store.feedback);

  return app;
};