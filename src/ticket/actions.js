const enliteApi = require('../utils/enlite-api');

/*
  {
    store_id: "",
    ticket: {
    //
    }
  }
*/
const createQuick = async (req, res, next) => {
  try {
    const { store_id, ticket } = req.body;
    const token = req.headers['authorization'];

    const newTicket = await enliteApi.createQuickTicket(store_id, ticket, token);

    res.json(newTicket);
  }
  catch (err) {
    next(err);
  }
};

const getStatus = async (req, res, next) => {
  try {
    const { store_id, firm_id } = req.params;
    const token = req.headers['authorization'];

    const status = await enliteApi.getInvoiceStatus(store_id, firm_id, token);

    res.json(status);
  }
  catch (err) {
    next(err);
  }
};

/*
  {
    store_id: "",
    firm_id: "",
    status_id: ""
  }
*/
const updateStatus = async (req, res, next) => {
  try {
    const { invoice_id } = req.params;
    const { store_id, firm_id, status_id } = req.body;
    const token = req.headers['authorization'];

    const status = await enliteApi.setInvoiceStatus(store_id,firm_id,invoice_id,status_id);
    
    res.json(status);
  }
  catch (err) {
    next(err);
  }
};

const addNote = async (req, res, next) => {
  try {
    const note = await enliteApi.createTicketNote();
    res.json(note);
  }
  catch (err) {
    next(err);
  }
};

//
module.exports = {
  createQuick,
  getStatus,
  updateStatus,
  addNote
};

