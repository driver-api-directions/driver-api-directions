const express = require('express');
const ticket = require('./actions');
const app = express.Router();

//
module.exports = () => {

  // Create Quick Ticket
  app.post('/quick', ticket.createQuick);

  // Get Invoice Status
  app.get('/invoiceStatus/:firm_id/:store_id', ticket.getStatus);

  // Update Invoice Status
  app.put('/invoiceStatus/:invoice_id', ticket.updateStatus);

  // Create Note
  app.post('/ticketNote', ticket.addNote);

  return app;
};