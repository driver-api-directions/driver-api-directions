const enliteApi = require('../utils/enlite-api');

/*
  body:{ 
    email: "baher@darkpos.com" 
  }
*/
const auth = async (req, res, next) => {
  try {
    const { email } = req.body;

    const response = await enliteApi.emailAuthentication(email);

    res.json(response);
  }
  catch (err) {
    next(err);
  }
};

/*
  body: { 
    email: "baher@darkpos.com", 
    code: "13905" 
  }
*/
const verify = async (req, res, next) => {
  try {
    const { email, code } = req.body;

    const response = await enliteApi.verifyCode(code, email);
    /*
      get the store and customer information then instead of stores, retuen them.
      think about multi store
    */

    res.json(response);
  }
  catch (err) {
    next(err);
  }
};

/*
  body: { 
    email: "baher@darkpos.com", 
    password: "13905" 
  }
*/
const ownerLogin = async (req, res, next) => {
  try {
    const { email, password } = req.body;

    const response = await enliteApi.ownerLogin({ email, password });
    
    res.json(response);
  } catch (err) {
    next(err);
  }
};

/*
  {   
    store_id: "Bx769"
  }
*/
//TODO: when its  needed, use it
// const checkRouteLicense = async (req, res, next) => {
//   try{
//     const { store_id } = req.body;
//     let active = false;

//     let licenses = await checkRouteLicense(store_id);
//     licenses = _.filter(licenses, { 'license_code': 'routing' });

//     if(licenses.length >= 1)
//       active = true;

//     res.json({active});
//   }
//   catch(err) {
//     next(err);
//   }
// };

//
module.exports = {
  auth,
  verify,
  ownerLogin,
};

