const express = require('express');
const session = require('./actions');
const app = express.Router();

//
module.exports = () => {

  // Email Authentication
  app.post('/emailAuth', session.auth); 

  // Verify Code
  app.post('/verifyCode', session.verify);

  // Login
  app.post('/login', session.ownerLogin);
  app.post('/ownerLogin', session.ownerLogin);

  return app;
};