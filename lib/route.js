const express = require('express');
const router = express.Router();
const os = require('os');
const config = require('../config');

module.exports = () => {

    // Health Check
    router.get('/health-check', (req,res) => {
        res.json({
            hostname: os.hostname(),
            environment: config.env
        });
    });

    // Featured Routes
    router.use('/customer', require('../src/customer')());
    router.use('/session', require('../src/session')());
    router.use('/store', require('../src/store')());
    router.use('/ticket', require('../src/ticket')());

    return router;
}
