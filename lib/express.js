const express = require('express');
const bodyParser = require('body-parser');
const routes = require('./route');
const app = express();

module.exports = () => {
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());

  app.use('/', routes());

  // Common Error handling
  app.use((err, req, res, next) => {
    console.error(err.message);

    res
      .status(err.statusCode || 200)
      .json({
        status: 'error',
        message: err.message
      });
  });

  return app;
};
