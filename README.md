## Quick Start

```
npm install
serverless offline start
  - OR - 
sls offline start
``` 

## Service Install

1. Install dependencies
```
npm install serverless -g
npm install
```
2. Configure AWS credentials
3. Create service (optional - run only once)
```
npm run create:dev
```
4. Deploy service
```
npm run deploy:dev
```

## Service Deployment

1. Configure the default AWS profile  (`serverless config credentials --provider aws --key AKIAILTYSBHYOYAXJ5UQ --secret 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`)
- These access key can be found in (https://console.aws.amazon.com/iam/home?region=us-east-1#/users/serverless-build?section=security_credentials)
2. Install Python, pip, and the AWS CLI (https://docs.aws.amazon.com/cli/latest/userguide/installing.html)
(https://docs.aws.amazon.com/cli/latest/userguide/awscli-install-windows.html#install-msi-on-windows)
3. pip install awscli
4. Verify service has been created (`npm run create:dev`)
5. Push branch and let Gitlab CI runner push serverless package to AWS (`npm run deploy:dev`)
6. Configure darkpos.io certificate (f0c299fd): https://console.aws.amazon.com/apigateway
7. Configure CloudFlare CNAME and Page Rule to enforce SSL:Full
8. Verify service is up by checking: https://driver-api-directions.darkpos.io/health-check

## Links

https://stackoverflow.com/questions/36737313/how-to-cname-to-amazon-api-gateway-endpoint?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa
