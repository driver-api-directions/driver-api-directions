//
const config = () => {
  const env = process.env.NODE_ENV || 'development';
  const environment = {
    development: {
      port: process.env.PORT || 8080,
      api: 'https://dev.enlitecloud.com/api2',
      env: 'dev',
      brand: 'enlite',
      reqParams: {},
      language: 'en'
    },
    production: {
      port: process.env.PORT || 8080,
      api: 'https://www.enlitecloud.com/live/api2',
      env: 'prod',
      brand: 'enlite',
      reqParams: {},
      language: 'en'
    }
  };

  return environment[env];
}

//
module.exports = config();
