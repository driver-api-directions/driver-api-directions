const config = require('./config');
const express = require('./lib/express');
const service = express();

//
service.listen(config.port, () => {
  console.log('-----------------------------------------------------');
  console.log(` ENV       : ${config.env}`);
  console.log(` PORT      : ${config.port}`);
  console.log(` STARTED   : ${new Date()}`);
  console.log('-----------------------------------------------------');
});

module.exports = { service };
